let num1 = +(prompt('Enter num1'));
let num2 = +(prompt('Enter num2'));


while (isNaN(num1) || isNaN(num2)) {
    num1 = prompt("Enter correct first number", num1);
    num2 = prompt("Enter correct second number", num2);
}
let operation = prompt("Enter operation");
alert(DoSomeMath(num1, num2, operation));

function DoSomeMath(num1, num2, operation) {
    let result = 0;
    switch (operation) {
        case '+':

            result = num1 + num2;
            break;
        case '-':

            result = num1 - num2;
            break;

        case '/':

            result = num1 / num2;
            break;

        case '*':

            result = num1 * num2;
            break;
    }
    return result;
}