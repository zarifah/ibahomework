1.Describe in your own words what the functions in programming are for.

Quite often we need to perform a similar action in many places of the script.
A JavaScript function is executed when "something" calls it.
Basically, a function is a set of statements that performs some tasks or
 does some calculation and then returns the result to the user.
A JavaScript function is defined with the function keyword, followed by a name, followed by parentheses ().
Function names can contain letters, digits, underscores, and dollar signs (same rules as variables).

The parentheses may include parameter names separated by commas:
(parameter1, parameter2, ...)
A function can have one or more parameters, which will be supplied by the calling code and 
can be used inside a function. JavaScript is a dynamic type scripting language, 
so a function parameter can have value of any data type.
Example:
function sayHello() {
         alert("Hello there");

2.Describe in your own words what is the reason why arguments are defined in a function.
 Why are they passed when a function is called?

JavaScript functions have a built-in object called the arguments object.
The argument object contains an array of the arguments used when the function was called. 
Arguments, on the other hand, are the values the function receives from each parameter
when the function is executed . 
example:
const param1 = true;
const param2 = false;
function twoParams(param1, param2){
  console.log(param1, param2);
}
In the  example, our two arguments are true & false.
Here’s an example with two parameters — param1 and param2

Parameters are used when defining a function, they are the names created in the function definition. 
In fact, during a function definition, we can pass in up to 255 parameters! 
Parameters are separated by commas in the (). 

JavaScript arguments are passed by value: The function only gets to know the values, not the argument's locations.
If a function changes an argument's value, it does not change the parameter's original value.

"Changes to arguments are not visible outside the function."


