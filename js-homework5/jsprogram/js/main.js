let tabs = document.querySelector("#tabs");
let content = document.querySelector("#content");


for (let i = 0; i < content.children.length; i++) {
    tabs.children[i].dataset.index = i;
    if (i)
        content.children[i].hidden = true;
}

tabs.onclick = e => {
    tabs.querySelector(".active").classList.toggle("active");
    content.querySelector("li:not([hidden])").hidden = true;
    e.target.classList.toggle("active");
    content.children[e.target.dataset.index].hidden = false;
}