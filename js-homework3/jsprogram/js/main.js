function createNewUser() {
    let firstName = prompt("What is your name?");


    while (!firstName) {
        firstName = prompt("Please enter your name");
    }
    let lastName = prompt("What is your surname?");
    while (!lastName) {
        lastName = prompt("Please enter your surname");
    }

    let user = {

        name: firstName,
        surname: lastName,

        getLogin: function() {
            return this.name.charAt(0).toLowerCase() + this.surname.toLowerCase();
        }
    }

    return user;
}

const user = new createNewUser();

console.log(user.getLogin());